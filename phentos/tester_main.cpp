#include <iostream>
#include <string>
#include "femtos.hpp"

using namespace std;

unsigned num_iterations = 100000;
unsigned num_deps = 0;

void bogus (unsigned long long swID) {
}

unsigned long long * GLOBAL_ptr;

void FA_task01_printValAndUpdate (unsigned long long swID) {
	//cout << "[printValAndUpdate]: " << val << endl;
	*GLOBAL_ptr = *GLOBAL_ptr + 1;

	asm volatile ("fence" ::: "memory");
}

void printValAndUpdate (unsigned long long swID) {
	unsigned * ptr = (unsigned *) metadataArray[swID].depAddresses0[0];
	unsigned val = *ptr;

	//cout << "[printValAndUpdate]: " << val << endl;
	*ptr = val + 1;
}

unsigned long long testVar[15] = {
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};

void testDriver08 (void) {
	unsigned long long lastSWID = 0;
	unsigned numPendingWorkRequests = 0;

	asm volatile ("fence" ::: "memory");

	for (int i = 0; i < num_iterations; i++) {
		unsigned long long swID = getNewSWID(lastSWID);
		//cout << "[testDriver02]: Just got swID = " << swID << endl;

		metadataArray[swID].functionAddr = (unsigned long long) printValAndUpdate;
		metadataArray[swID].depAddresses0[0] = (unsigned long long) (unsigned long long) testVar;
		asm volatile ("fence" ::: "memory");

		//cout << "[testDriver01]: Just got swID = " << swID << endl;
#ifdef SCHEDULING_HEURISTIC
		make_submission_request_or_work(3 * (1 + num_deps), num_threads - 1, numPendingWorkRequests);
#else
		make_submission_request_or_work(3 * (1 + num_deps), 0, numPendingWorkRequests);
#endif
		
		submit_three_or_work(swID, num_deps, numPendingWorkRequests);
		for (int j = 0; j < num_deps; j++) {
			submit_three_or_work((unsigned long long) (&(testVar[j])), 2, numPendingWorkRequests);
		}

		lastSWID = swID;
	}	

	task_wait_and_try_executing_tasks(num_iterations);
}

void testDriver07 (void) {
	unsigned long long lastSWID = 0;
	unsigned numPendingWorkRequests = 0;

	asm volatile ("fence" ::: "memory");

	for (int i = 0; i < num_iterations; i++) {
		unsigned long long swID = getNewSWID(lastSWID);
		//cout << "[testDriver02]: Just got swID = " << swID << endl;

		metadataArray[swID].functionAddr = (unsigned long long) bogus;
		asm volatile ("fence" ::: "memory");

		//cout << "[testDriver01]: Just got swID = " << swID << endl;
#ifdef SCHEDULING_HEURISTIC
		make_submission_request_or_work(3 * (1 + num_deps), num_threads - 1, numPendingWorkRequests);
#else
		make_submission_request_or_work(3 * (1 + num_deps), 0, numPendingWorkRequests);
#endif
		
		submit_three_or_work(swID, num_deps, numPendingWorkRequests);
		for (int j = 0; j < num_deps; j++) {
			submit_three_or_work((unsigned long long) (&(testVar[j])), 0, numPendingWorkRequests);
		}

		lastSWID = swID;
	}	

	task_wait_and_try_executing_tasks(num_iterations);
}

void testDriver06 (void) {
	unsigned long long lastSWID = 0;
	unsigned numPendingWorkRequests = 0;

	GLOBAL_ptr = testVar;
	functionAddresses[0] = (unsigned long long) bogus;

	asm volatile ("fence" ::: "memory");

	for (int i = 0; i < num_iterations; i++) {
		//cout << "[testDriver01]: Just got swID = " << swID << endl;
#ifdef SCHEDULING_HEURISTIC
		make_submission_request_or_work_fast(3 * (1 + num_deps), num_threads - 1, numPendingWorkRequests);
#else
		make_submission_request_or_work_fast(3 * (1 + num_deps), 0, numPendingWorkRequests);
#endif
		
		// The number first parameter "1" is referencing the task's
		// function pointer
		submit_three_or_work_fast(1, num_deps, numPendingWorkRequests);
		for (int j = 0; j < num_deps; j++) {
			submit_three_or_work_fast((unsigned long long) (&(testVar[j])), 0, numPendingWorkRequests);
		}
	}	

	fast_task_wait_and_try_executing_tasks(num_iterations);
}

void testDriver05 (void) {
	unsigned long long lastSWID = 0;
	unsigned numPendingWorkRequests = 0;

	GLOBAL_ptr = testVar;
	functionAddresses[0] = (unsigned long long) FA_task01_printValAndUpdate;

	asm volatile ("fence" ::: "memory");

	for (int i = 0; i < num_iterations; i++) {
		//cout << "[testDriver01]: Just got swID = " << swID << endl;
#ifdef SCHEDULING_HEURISTIC
		make_submission_request_or_work_fast(3 * (1 + num_deps), num_threads - 1, numPendingWorkRequests);
#else
		make_submission_request_or_work_fast(3 * (1 + num_deps), 0, numPendingWorkRequests);
#endif
		
		// The number first parameter "1" is referencing the task's
		// function pointer
		submit_three_or_work_fast(1, num_deps, numPendingWorkRequests);
		for (int j = 0; j < num_deps; j++) {
			submit_three_or_work_fast((unsigned long long) (&(testVar[j])), 2, numPendingWorkRequests);
		}
	}	

	fast_task_wait_and_try_executing_tasks(num_iterations);
}

void testDriver04 (void) {
	unsigned long long lastSWID = 0;
	unsigned numPendingWorkRequests = 0;

	functionAddresses[0] = (unsigned long long) bogus;
	asm volatile ("fence" ::: "memory");

	for (int i = 0; i < num_iterations; i++) {
		//unsigned long long swID = getNewSWID(lastSWID);
		//cout << "[testDriver02]: Just got swID = " << swID << endl;

		//metadataArray[swID].functionAddr = (unsigned long long) bogus;

		//asm volatile ("fence" ::: "memory");

#ifdef SCHEDULING_HEURISTIC
		make_submission_request_or_work_fast(3, num_threads - 1, numPendingWorkRequests);
#else
		make_submission_request_or_work_fast(3, 0, numPendingWorkRequests);
#endif
		submit_three_or_work_fast(1, 0, numPendingWorkRequests);
		
		//lastSWID = swID;
	}	

	fast_task_wait_and_try_executing_tasks(num_iterations);
}

void testDriver03 (void) {
	unsigned long long lastSWID = 0;
	unsigned numPendingWorkRequests = 0;

	GLOBAL_ptr = testVar;
	functionAddresses[0] = (unsigned long long) FA_task01_printValAndUpdate;

	asm volatile ("fence" ::: "memory");

	for (int i = 0; i < num_iterations; i++) {
		//cout << "[testDriver01]: Just got swID = " << swID << endl;
#ifdef SCHEDULING_HEURISTIC
		make_submission_request_or_work_fast(6, num_threads - 1, numPendingWorkRequests);
#else
		make_submission_request_or_work_fast(6, 0, numPendingWorkRequests);
#endif
		submit_three_or_work_fast(1, 1, numPendingWorkRequests);
		submit_three_or_work_fast((unsigned long long) testVar, 2, numPendingWorkRequests);
	}	

	fast_task_wait_and_try_executing_tasks(num_iterations);
}

void testDriver02 (void) {
	unsigned long long lastSWID = 0;
	unsigned numPendingWorkRequests = 0;

	for (int i = 0; i < num_iterations; i++) {
		unsigned long long swID = getNewSWID(lastSWID);
		//cout << "[testDriver02]: Just got swID = " << swID << endl;

		metadataArray[swID].functionAddr = (unsigned long long) bogus;
		asm volatile ("fence" ::: "memory");

#ifdef SCHEDULING_HEURISTIC
		make_submission_request_or_work(3, num_threads - 1, numPendingWorkRequests);
#else
		make_submission_request_or_work(3, 0, numPendingWorkRequests);
#endif
		submit_three_or_work(swID, 0, numPendingWorkRequests);
		
		lastSWID = swID;
	}	

	task_wait_and_try_executing_tasks(num_iterations);
}

void testDriver01 (void) {
	unsigned long long lastSWID = 0;
	unsigned numPendingWorkRequests = 0;

	for (int i = 0; i < num_iterations; i++) {
		unsigned long long swID = getNewSWID(lastSWID);
		//cout << "[testDriver01]: Just got swID = " << swID << endl;

		metadataArray[swID].functionAddr = (unsigned long long) printValAndUpdate;
		metadataArray[swID].depAddresses0[0] = (unsigned long long) (unsigned long long) testVar;

		asm volatile ("fence" ::: "memory");

#ifdef SCHEDULING_HEURISTIC
		make_submission_request_or_work(6, num_threads - 1, numPendingWorkRequests);
#else
		make_submission_request_or_work(6, 0, numPendingWorkRequests);
#endif
		submit_three_or_work(swID, 1, numPendingWorkRequests);
		submit_three_or_work((unsigned long long) testVar, 2, numPendingWorkRequests);
		
		lastSWID = swID;
	}	

	task_wait_and_try_executing_tasks(num_iterations);
}

int main (int argc, char ** argv) {
	unsigned long long global_start, global_start_after_init, global_end;
	bool verbose = false;

	if (verbose) {
		cout << "Size of task_metadata (bytes): " << sizeof(struct task_metadata) << endl;
	}

	void (*testDrivers[])(void) = {testDriver01, testDriver02, testDriver03, testDriver04, testDriver05, testDriver06, testDriver07, testDriver08};
	unsigned test_driver_id = 0;

	if (argc > 1) {
		test_driver_id = stoi(argv[1]);
		if (verbose) {
			cout << "Preparing to call test driver #" << test_driver_id << endl;
		}
	}
	if (argc > 2) {
		num_iterations = stoi(argv[2]);
	}

	if (argc > 3) {
		num_deps = stoi(argv[3]);
	}

	if (argc > 4) {
		verbose = true;
	}

	if (verbose) {
		cout << "Number of iterations to run: " << num_iterations << endl;
		cout << "Considering the system to have the following number of threads: " << num_threads << endl;
	}

	global_start = query_cycle_count();

	switch (test_driver_id) {
		case 0:
		case 1:
		case 6:
		case 7:		femtos_init();
							break;
		default:	femtos_fast_init();
							break;
	}

	global_start_after_init = query_cycle_count();

	testDrivers[test_driver_id]();

	global_end = query_cycle_count();

	if (verbose) {
		cout << "Number of cycles elapsed:\t\t" << global_end - global_start << endl;
		cout << "Number of cycles since initialization completed:\t\t" << global_end - global_start_after_init << endl;
		cout << "Number of cycles per iteration:\t\t" << (global_end - global_start) / (double) num_iterations << endl;
		cout << "Number of cycles per iteration, not considering initialization:\t\t" << (global_end - global_start_after_init) / (double) num_iterations << endl;
	}

	return 0;
}
