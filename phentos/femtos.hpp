#include <climits>
#include <cstdlib>
#include <atomic>
#include <string>
#include <iostream>
#include <pthread.h>

#ifndef NUM_DIFFERENT_TASKS
#define NUM_DIFFERENT_TASKS 8
#endif

#define METADATA_ARRAY_SIZE 512
#define FAILS_BEFORE_NUM_RETIRED_UPDATE 1024
#define INIT_THREADS_NOP_LOPP_SIZE 1000
#define DEFAULT_NUM_THREADS 8
#define DEFAULT_NUM_DISABLED_THREADS 0

#define CUSTOM2 0x5b
#define CUSTOM3 0x7b
#define STR(x) #x
#define XSTR(s) STR(s)

struct task_metadata {
	uint64_t functionAddr;	
	uint64_t depAddresses0[7];
#ifdef EXTRA_DEPS
	uint64_t depAddresses1[8];
#endif
} __attribute__ ((aligned (64)));

typedef struct task_metadata TaskMetadata;

struct profiling_info {
	uint64_t numberOfTasksExecuted;
	uint64_t executedTaskCycles;
	uint64_t padding[6];
} __attribute__ ((aligned (64) ));

typedef struct task_metadata ProfilingInfo;

TaskMetadata __attribute__((aligned(64))) metadataArray[METADATA_ARRAY_SIZE];

std::atomic<long int> numInitializedThreads{0};
std::atomic<unsigned> numRetiredTasks{0};
unsigned numSubmittedTasks = 0;
unsigned num_threads = 8;
unsigned num_disabled_threads = 0;
uint64_t functionAddresses[NUM_DIFFERENT_TASKS];

pthread_t threads[DEFAULT_NUM_THREADS];

#ifdef ENABLE_PROFILING
ProfilingInfo threadSpecificProfilingInfo[DEFAULT_NUM_THREADS];
std::atomic<unsigned long long> totalExecutedTaskCycles{0};
#endif

inline void nopLoop (unsigned long long loopSize) {
	for (unsigned long long i = 0; i < loopSize; i++) {
		asm volatile ("nop" ::: "memory");
	}
}

inline unsigned long long query_cycle_count() {
	unsigned long long r;

	asm volatile ("fence" ::: "memory");
	asm volatile ("rdcycle %0" : "=r"(r) :: "memory");
	asm volatile ("fence" ::: "memory");

	return r;
}

inline short CorePin(int coreID)
{
	short status=0;

	//int nThreads = sys.getNumThreads();

	//std::cout<<nThreads;
	cpu_set_t set;
	//std::cout<<"\nPinning to Core:"<<coreID<<"\n";
	CPU_ZERO(&set);

	if(coreID == -1)
	{
		status=-1;
		std::cerr<<"CoreID is -1"<<"\n";
		return status;
	}

	/*
	   if(coreID > nThreads)
	   {
	   std::cerr<<"Invalid CORE ID"<<"\n";
	   return status;
	   }
	 */

#ifdef SCHEDULING_HEURISTIC
	CPU_SET(num_threads - (coreID + 1),&set);
#else
	CPU_SET(coreID,&set);
#endif
	if(sched_setaffinity(0, sizeof(cpu_set_t), &set) < 0)
	{
		std::cerr<<"Unable to Set Affinity"<<"\n";
		return -1;
	}
	return 1;
}

inline unsigned getNewSWID (const unsigned firstIDNotToBeUsed) {
	unsigned swID = firstIDNotToBeUsed + 1;

	while (true) {
		swID = swID % METADATA_ARRAY_SIZE;
		if (metadataArray[swID].functionAddr == 0) {
			return swID;
		}
		
#ifndef NO_FENCES
		//asm volatile ("fence" ::: "memory");
#endif
		swID++;
	}
}

inline unsigned long long get_picosID() {
	unsigned long long resp = 0;

	//	type opcode f3 f7 rd rs1 rs2
	asm volatile (".insn r " XSTR(CUSTOM2) ", 0x7, 0x4, %[rd], %[rs1], %[rs2]" : [rd]"=r"(resp) : [rs1]"r"(resp), [rs2]"r"(resp));

	return resp;
}

inline unsigned long long get_nanosID() {
	unsigned long long resp = 0;

	//               opcode  rd      rs1  rs2 funct 

	//	type opcode f3 f7 rd rs1 rs2
	asm volatile (".insn r " XSTR(CUSTOM2) ", 0x7, 0x2, %[rd], %[rs1], %[rs2]" : [rd]"=r"(resp) : [rs1]"r"(resp), [rs2]"r"(resp));

	return resp;
}

inline unsigned long long make_work_request(int coreID = 0) {
	int resp = 0;

	//	type opcode f3 f7 rd rs1 rs2
	asm volatile (".insn r " XSTR(CUSTOM2) ", 0x7, 0x6, %[rd], %[rs1], %[rs2]" : [rd]"=r"(resp) : [rs1]"r"(resp), [rs2]"r"(resp));
	//std::cerr << "return value of make_work_request: " << resp << std::endl;

	return resp;
}

inline unsigned long long make_submission_request(int numPackets, int swID) {
	unsigned long long resp = 0;

	//                     opcode  rs1         funct   
	//	type opcode f3 f7 rd rs1 rs2
	asm volatile (".insn r " XSTR(CUSTOM2) ", 0x7, 0x5, %[rd], %[rs1], %[rs2]" : [rd]"=r"(resp) : [rs1]"r"(numPackets), [rs2]"r"(swID));

	return resp;
}

inline unsigned long long submit_packet(unsigned long long packet, int swID) {
	unsigned long long resp = 0;

	//                     opcode  rs1     funct   
	//	type opcode f3 f7 rd rs1 rs2
	asm volatile (".insn r " XSTR(CUSTOM2) ", 0x7, 0x1, %[rd], %[rs1], %[rs2]" : [rd]"=r"(resp) : [rs1]"r"(packet), [rs2]"r"(swID));

	return resp;
}

inline unsigned long long submit_three_packets(unsigned long long bigPacket, unsigned long long smallPacket) {
	unsigned long long resp = 0;
	//                     opcode  rs1     funct   
	//ROCC_INSTRUCTION_0_R_0(3,      packet, 1,     10);
	//
	//	type opcode f3 f7 rd rs1 rs2
	asm volatile (".insn r " XSTR(CUSTOM2) ", 0x7, 0x7, %[rd], %[rs1], %[rs2]" : [rd]"=r"(resp) : [rs1]"r"(bigPacket), [rs2]"r"(smallPacket));

	return resp;
}

inline void retire_task(unsigned long picosID) {
	//                     opcode  rs1     funct   
	int filler = 0;

	//ROCC_INSTRUCTION_0_R_0(3,      picosID, 3,     10);
	//	type opcode f3 f7 rd rs1 rs2
	asm volatile (".insn r " XSTR(CUSTOM2) ", 0x2, 0x3, %[rd], %[rs1], %[rs2]" : [rd]"=r"(filler) : [rs1]"r"(picosID), [rs2]"r"(filler));
}

inline void finish_task (unsigned long long swID, unsigned long long picosID) {
	metadataArray[swID].functionAddr = 0;
#ifndef NO_FENCES
	asm volatile ("fence" ::: "memory");
#endif
	retire_task(picosID);
}

inline void finish_dependence_less_task (unsigned long long picosID) {
	retire_task(picosID);
}

inline unsigned long long core_fast_try_fetching_and_executing () {
	unsigned long long swID = get_nanosID();
	unsigned long long picosID;
#ifdef ENABLE_PROFILING
	unsigned long long totalLocalExecutedCycles = 1;
	unsigned long long start = 0;
	unsigned long long stop = 0;
#endif

	if (swID < ULLONG_MAX) {
		do {
			picosID = get_picosID();
		} while (picosID == ULLONG_MAX);

		void (*funcPtr)(unsigned long long int);
		unsigned taskID = (swID >> 1) & 0x7;

		//std::cout << "[core_fast_try_fetching_and_executing]: Going to try fetching the funcPtr for task with id: " << taskID;

		funcPtr = (void (*)(unsigned long long int)) functionAddresses[taskID];

		//std::cout << "[core_fast_try_fetching_and_executing]: Fetched function addres = " << funcPtr;

#ifdef ENABLE_PROFILING 
		start = query_cycle_count();
		funcPtr(swID);
		stop = query_cycle_count();
		totalLocalExecutedCycles += (stop - start);
#else
		funcPtr(swID);
#endif
		finish_dependence_less_task(picosID);

#ifdef ENABLE_PROFILING
		return totalLocalExecutedCycles;
#else
		return 1;
#endif
	}

	return 0;
}

inline unsigned long long fast_try_fetching_and_executing (unsigned & numPendingWorkRequests) {
	unsigned long long localExecutedCycles;

	if (!numPendingWorkRequests) {
		numPendingWorkRequests = make_work_request();
	}

	localExecutedCycles = core_fast_try_fetching_and_executing();

	if (localExecutedCycles) {
		numPendingWorkRequests--;
		//std::cout << "[fast_try_fetching_and_executing]: Return value: " << localExecutedCycles << std::endl;
		return localExecutedCycles;
	}

	//std::cout << "[fast_try_fetching_and_executing]: Return value: " << 0 << std::endl;
	return 0;
}

inline unsigned long long core_try_fetching_and_executing () {
	unsigned long long swID = get_nanosID();
	unsigned long long picosID;
#ifdef ENABLE_PROFILING
	unsigned long long totalLocalExecutedCycles = 1;
	unsigned long long start = 0;
	unsigned long long stop = 0;
#endif

	if (swID < ULLONG_MAX) {
		do {
			picosID = get_picosID();
		} while (picosID == ULLONG_MAX);

		void (*funcPtr)(unsigned long long int);

		bool taskIsDependenceLess = false;

		if (taskIsDependenceLess) {
			funcPtr = (void (*)(unsigned long long int)) swID;

#ifdef ENABLE_PROFILING 
			start = query_cycle_count();
			funcPtr(swID);
			stop = query_cycle_count();
			totalLocalExecutedCycles += (stop - start);
#else
			funcPtr(swID);
#endif
			finish_dependence_less_task(picosID);
		} else {
#ifndef NO_FENCES
			asm volatile ("fence" ::: "memory");
#endif
			funcPtr = (void (*)(unsigned long long int)) metadataArray[swID].functionAddr;

#ifdef ENABLE_PROFILING 
			start = query_cycle_count();
			funcPtr(swID);
			stop = query_cycle_count();
			totalLocalExecutedCycles += (stop - start);
#else
			funcPtr(swID);
#endif
			finish_task(swID, picosID);
		}

#ifdef ENABLE_PROFILING
		//std::cout << "[core_try_fetching_and_executing]: Return value: " << totalLocalExecutedCycles << std::endl;
		return totalLocalExecutedCycles;
#else
		//std::cout << "[core_try_fetching_and_executing]: Return value: " << 1 << std::endl;
		return 1;
#endif
	}

	//std::cout << "[core_try_fetching_and_executing]: Return value: " << 0 << std::endl;
	return 0;
}

inline unsigned long long try_fetching_and_executing (unsigned & numPendingWorkRequests) {
	unsigned long long localExecutedCycles;

	if (!numPendingWorkRequests) {
		numPendingWorkRequests = make_work_request();
	}

	localExecutedCycles = core_try_fetching_and_executing();

	if (localExecutedCycles) {
		numPendingWorkRequests--;
		//std::cout << "[try_fetching_and_executing]: Return value: " << localExecutedCycles << std::endl;
		return localExecutedCycles;
	}

	//std::cout << "[try_fetching_and_executing]: Return value: " << 0 << std::endl;
	return 0;
}

inline void task_wait (unsigned numberOfSubmittedTasks) {
	while(numRetiredTasks < numberOfSubmittedTasks) {
		nopLoop(100);
	}
}

inline void fast_task_wait_and_try_executing_tasks (unsigned long long numberOfSubmittedTasks) {
	int numLocallyRetiredTasks = 0;
	unsigned failCounter = 0;
	unsigned numPendingWorkRequests = 0;
	unsigned long long executedTaskCycles;
#ifdef ENABLE_PROFILING
	unsigned long long totalLocalExecutedCycles = 0;
#endif

	while(numRetiredTasks < numberOfSubmittedTasks) {

		executedTaskCycles = fast_try_fetching_and_executing(numPendingWorkRequests);

		if (executedTaskCycles > 0) {
			numLocallyRetiredTasks++;
#ifdef ENABLE_PROFILING
			totalLocalExecutedCycles += executedTaskCycles;
#endif
		} else {
			if (((failCounter++) % FAILS_BEFORE_NUM_RETIRED_UPDATE == 0) && numLocallyRetiredTasks) {
				numRetiredTasks += numLocallyRetiredTasks;
				numLocallyRetiredTasks = 0;
#ifdef ENABLE_PROFILING
				totalExecutedTaskCycles += totalLocalExecutedCycles;
				totalLocalExecutedCycles = 0;
#endif
			}
		}
	}
}

inline void task_wait_and_try_executing_tasks (unsigned long long numberOfSubmittedTasks) {
	int numLocallyRetiredTasks = 0;
	unsigned failCounter = 0;
	unsigned numPendingWorkRequests = 0;
	unsigned long long executedTaskCycles;
#ifdef ENABLE_PROFILING
	unsigned long long totalLocalExecutedCycles = 0;
#endif

	//std::cout << "[task_wait_and_try_executing_tasks]: Just entered the function. We are going to wait until " << numberOfSubmittedTasks << " tasks are retired." <<  std::endl;

	while(numRetiredTasks < numberOfSubmittedTasks) {

		executedTaskCycles = try_fetching_and_executing(numPendingWorkRequests);

		if (executedTaskCycles > 0) {
			numLocallyRetiredTasks++;
#ifdef ENABLE_PROFILING
			totalLocalExecutedCycles += executedTaskCycles;
#endif
		} else {
			if (((failCounter++) % FAILS_BEFORE_NUM_RETIRED_UPDATE == 0) && numLocallyRetiredTasks) {
				//std::cout << "[task_wait_and_try_executing_tasks]: Updating numRetiredTasks from " << numRetiredTasks << " to " << numRetiredTasks + numLocallyRetiredTasks << std::endl;
				numRetiredTasks += numLocallyRetiredTasks;
				numLocallyRetiredTasks = 0;
#ifdef ENABLE_PROFILING
				totalExecutedTaskCycles += totalLocalExecutedCycles;
				totalLocalExecutedCycles = 0;
#endif
			}
		}
	}
}

void * FastWorkerRoutine(void *threadid)
{
	long tid;
	tid = (long)threadid;
	//printf("Hello World! It's me, worker #%ld!\n", tid);
	CorePin(tid);

	unsigned long long executedTaskCycles;
#ifdef ENABLE_PROFILING
	unsigned long long totalLocalExecutedCycles = 0;
#endif

	numInitializedThreads++;
	while (numInitializedThreads < num_threads) {
		nopLoop(INIT_THREADS_NOP_LOPP_SIZE);
	}

	int numLocallyRetiredTasks = 0;
	unsigned numPendingWorkRequests = 0;
	unsigned failCounter = 0;

	while (true) {
		executedTaskCycles = fast_try_fetching_and_executing(numPendingWorkRequests);
		if (executedTaskCycles > 0) {
#ifdef ENABLE_PROFILING
			totalLocalExecutedCycles += executedTaskCycles;
#endif
			numLocallyRetiredTasks++;
		} else {
			if (((failCounter++) % FAILS_BEFORE_NUM_RETIRED_UPDATE == 0) && numLocallyRetiredTasks) {
				numRetiredTasks += numLocallyRetiredTasks;
				numLocallyRetiredTasks = 0;
#ifdef ENABLE_PROFILING
				totalExecutedTaskCycles += totalLocalExecutedCycles;
				totalLocalExecutedCycles = 0;
#endif
			}
		}
	}

	pthread_exit(NULL);
}

void * FastDisabledWorkerRoutine(void *threadid)
{
	long tid;
	tid = (long)threadid;
	//printf("Hello World! It's me, worker #%ld!\n", tid);
	CorePin(tid);

	unsigned long long executedTaskCycles;
#ifdef ENABLE_PROFILING
	unsigned long long totalLocalExecutedCycles = 0;
#endif

	numInitializedThreads++;
	while (numInitializedThreads < num_threads) {
		nopLoop(INIT_THREADS_NOP_LOPP_SIZE);
	}

	int numLocallyRetiredTasks = 0;
	unsigned failCounter = 0;

	while (true) {
		executedTaskCycles = core_fast_try_fetching_and_executing();
		if (executedTaskCycles > 0) {
#ifdef ENABLE_PROFILING
			totalLocalExecutedCycles += executedTaskCycles;
#endif
			numLocallyRetiredTasks++;
		} else {
			if (((failCounter++) % FAILS_BEFORE_NUM_RETIRED_UPDATE == 0) && numLocallyRetiredTasks) {
				numRetiredTasks += numLocallyRetiredTasks;
				numLocallyRetiredTasks = 0;
#ifdef ENABLE_PROFILING
				totalExecutedTaskCycles += totalLocalExecutedCycles;
				totalLocalExecutedCycles = 0;
#endif
			}
		}
	}

	pthread_exit(NULL);
}

void * WorkerRoutine(void *threadid)
{
	long tid;
	tid = (long)threadid;
	//printf("Hello World! It's me, worker #%ld!\n", tid);
	CorePin(tid);

	unsigned long long executedTaskCycles;
#ifdef ENABLE_PROFILING
	unsigned long long totalLocalExecutedCycles = 0;
#endif

	numInitializedThreads++;
	while (numInitializedThreads < num_threads) {
		nopLoop(INIT_THREADS_NOP_LOPP_SIZE);
	}

	int numLocallyRetiredTasks = 0;
	unsigned numPendingWorkRequests = 0;
	unsigned failCounter = 0;

	while (true) {
		executedTaskCycles = try_fetching_and_executing(numPendingWorkRequests);
		if (executedTaskCycles > 0) {
#ifdef ENABLE_PROFILING
			totalLocalExecutedCycles += executedTaskCycles;
#endif
			numLocallyRetiredTasks++;
		} else {
			if (((failCounter++) % FAILS_BEFORE_NUM_RETIRED_UPDATE == 0) && numLocallyRetiredTasks) {
				numRetiredTasks += numLocallyRetiredTasks;
				numLocallyRetiredTasks = 0;
#ifdef ENABLE_PROFILING
				totalExecutedTaskCycles += totalLocalExecutedCycles;
				totalLocalExecutedCycles = 0;
#endif
			}
		}
	}

	pthread_exit(NULL);
}

void * DisabledWorkerRoutine(void *threadid)
{
	long tid;
	tid = (long)threadid;
	//printf("Hello World! It's me, worker #%ld!\n", tid);
	CorePin(tid);

	unsigned long long executedTaskCycles;
#ifdef ENABLE_PROFILING
	unsigned long long totalLocalExecutedCycles = 0;
#endif

	numInitializedThreads++;
	while (numInitializedThreads < num_threads) {
		nopLoop(INIT_THREADS_NOP_LOPP_SIZE);
	}

	int numLocallyRetiredTasks = 0;
	unsigned failCounter = 0;

	while (true) {
		executedTaskCycles = core_try_fetching_and_executing();
		if (executedTaskCycles > 0) {
#ifdef ENABLE_PROFILING
			totalLocalExecutedCycles += executedTaskCycles;
#endif
			numLocallyRetiredTasks++;
		} else {
			if (((failCounter++) % FAILS_BEFORE_NUM_RETIRED_UPDATE == 0) && numLocallyRetiredTasks) {
				numRetiredTasks += numLocallyRetiredTasks;
				numLocallyRetiredTasks = 0;
#ifdef ENABLE_PROFILING
				totalExecutedTaskCycles += totalLocalExecutedCycles;
				totalLocalExecutedCycles = 0;
#endif
			}
		}
	}

	pthread_exit(NULL);
}

inline void initialize_fast_worker_threads(void) {
	CorePin(0);
	numInitializedThreads++;

	int rc;
	long t;
	for(t=1;t<num_threads;t++){
		//printf("In main: creating thread %ld\n", t);
#ifdef SCHEDULING_HEURISTIC
		if (t < num_disabled_threads) {
			rc = pthread_create(&threads[t], NULL, FastDisabledWorkerRoutine, (void *)t);
		} else {
			rc = pthread_create(&threads[t], NULL, FastWorkerRoutine, (void *)t);
		}
#else
		if (t < (num_threads - num_disabled_threads)) {
			rc = pthread_create(&threads[t], NULL, FastWorkerRoutine, (void *)t);
		} else {
			rc = pthread_create(&threads[t], NULL, FastDisabledWorkerRoutine, (void *)t);
		}
#endif
		if (rc){
			printf("ERROR; return code from pthread_create() is %d\n", rc);
			exit(-1);
		}
	}

	while(numInitializedThreads < num_threads){
		nopLoop(INIT_THREADS_NOP_LOPP_SIZE);
	}
}

inline void initialize_worker_threads(void) {
	CorePin(0);
	numInitializedThreads++;

	int rc;
	long t;
	for(t=1;t<num_threads;t++){
		//printf("In main: creating thread %ld\n", t);
#ifdef SCHEDULING_HEURISTIC
		if (t < num_disabled_threads) {
			rc = pthread_create(&threads[t], NULL, DisabledWorkerRoutine, (void *)t);
		} else {
			rc = pthread_create(&threads[t], NULL, WorkerRoutine, (void *)t);
		}
#else
		if (t < (num_threads - num_disabled_threads)) {
			rc = pthread_create(&threads[t], NULL, WorkerRoutine, (void *)t);
		} else {
			rc = pthread_create(&threads[t], NULL, DisabledWorkerRoutine, (void *)t);
		}
#endif
		if (rc){
			printf("ERROR; return code from pthread_create() is %d\n", rc);
			exit(-1);
		}
	}

	while(numInitializedThreads < num_threads){
		nopLoop(INIT_THREADS_NOP_LOPP_SIZE);
	}
}

inline unsigned get_number_of_disabled_threads () {
	char * num_disabled_threads_string = getenv("FEMTOS_NUM_DISABLED_THREADS");
	if (num_disabled_threads_string != NULL)
		return std::stoi(num_disabled_threads_string);
	else
		return DEFAULT_NUM_DISABLED_THREADS;
}

inline unsigned get_number_of_threads () {
	char * num_threads_string = getenv("FEMTOS_NUM_THREADS");
	if (num_threads_string != NULL)
		return std::stoi(num_threads_string);
	else
		return DEFAULT_NUM_THREADS;
}

inline void report_execution_stats() {
#ifdef ENABLE_PROFILING
	unsigned long long fixedTotalExecutedTaskCycles = totalExecutedTaskCycles - numRetiredTasks;
	std::cout << "[report_execution_stats]: Total number of executed tasks since system initialization: " << numRetiredTasks << std::endl;
	std::cout << "[report_execution_stats]: Total number of executed task cycles: " << fixedTotalExecutedTaskCycles << std::endl;
	std::cout << "[report_execution_stats]: Mean task size: " << ((double) fixedTotalExecutedTaskCycles) / ((double) numRetiredTasks) << std::endl;
#else
	std::cout << "[report_execution_stats]: Profiling was not enabled during Phentos compilation." << std::endl;
#endif
}

inline void femtos_fast_init() {
	for (int i = 0; i < METADATA_ARRAY_SIZE; i++) {
		metadataArray[i].functionAddr = 0;
	}
	num_threads = get_number_of_threads();
	num_disabled_threads = get_number_of_disabled_threads();
	initialize_fast_worker_threads();
	asm volatile ("fence" ::: "memory");
}

inline void femtos_init() {
	for (int i = 0; i < METADATA_ARRAY_SIZE; i++) {
		metadataArray[i].functionAddr = 0;
	}
	num_threads = get_number_of_threads();
	num_disabled_threads = get_number_of_disabled_threads();
	initialize_worker_threads();
	asm volatile ("fence" ::: "memory");
}

#ifdef ENABLE_PROFILING
	#define make_submission_request_or_work_fast(numPackets, swID, numPendingWorkRequests)  \
	{ \
		int numLocallyRetiredTasks = 0;\
		unsigned long long totalLocalExecutedCycles = 0;\
		unsigned long long executedTaskCycles;\
		while (true) { \
			unsigned long long reply = make_submission_request(numPackets, swID); \
			/*std::cerr << "[make_submission_request_or_work, T" << thread << "]: Just got signal" << reply << "." << std::endl;*/ \
			if (reply == 0) \
			break; \
			executedTaskCycles = fast_try_fetching_and_executing(numPendingWorkRequests);\
			if (executedTaskCycles > 0) {\
				numLocallyRetiredTasks++;\
				totalLocalExecutedCycles += executedTaskCycles;\
			}\
		} \
		if (numLocallyRetiredTasks > 0) {\
			numRetiredTasks += numLocallyRetiredTasks;\
			totalExecutedTaskCycles += totalLocalExecutedCycles;\
		}\
	}

	#define make_submission_request_or_work(numPackets, swID, numPendingWorkRequests)  \
	{ \
		int numLocallyRetiredTasks = 0;\
		unsigned long long totalLocalExecutedCycles = 0;\
		unsigned long long executedTaskCycles;\
		while (true) { \
			unsigned long long reply = make_submission_request(numPackets, swID); \
			/*std::cerr << "[make_submission_request_or_work, T" << thread << "]: Just got signal" << reply << "." << std::endl;*/ \
			if (reply == 0) \
			break; \
			executedTaskCycles = try_fetching_and_executing(numPendingWorkRequests);\
			if (executedTaskCycles > 0) {\
				numLocallyRetiredTasks++;\
				totalLocalExecutedCycles += executedTaskCycles;\
			}\
		} \
		if (numLocallyRetiredTasks > 0) {\
			numRetiredTasks += numLocallyRetiredTasks;\
			totalExecutedTaskCycles += totalLocalExecutedCycles;\
		}\
	}

	#define submit_or_work(packet, swID, numPendingWorkRequests) \
	{\
		int numLocallyRetiredTasks = 0;\
		unsigned long long totalLocalExecutedCycles = 0;\
		unsigned long long executedTaskCycles;\
		while (true) {\
			unsigned long long reply = submit_packet(packet, swID);\
			/*std::cerr << "[submit_or_work]: Just got signal" << reply << "." << std::endl;*/\
			if (reply == 0)\
			break;\
			executedTaskCycles = try_fetching_and_executing(numPendingWorkRequests);\
			if (executedTaskCycles > 0) {\
				numLocallyRetiredTasks++;\
				totalLocalExecutedCycles += executedTaskCycles;\
			}\
		} \
		if (numLocallyRetiredTasks > 0) {\
			numRetiredTasks += numLocallyRetiredTasks;\
			totalExecutedTaskCycles += totalLocalExecutedCycles;\
		}\
	}

	#define submit_three_or_work_fast(bigPacket, smallPacket, numPendingWorkRequests)  \
	{                                                               \
		int numLocallyRetiredTasks = 0;\
		unsigned long long totalLocalExecutedCycles = 0;\
		unsigned long long executedTaskCycles;\
		while (true) { \
			unsigned long long reply = submit_three_packets(bigPacket, smallPacket); \
			/*std::cerr << "[submit_three_or_work" << thread << "]: Just got signal" << reply << "." << std::endl;*/ \
			if (reply == 0) \
			break; \
			executedTaskCycles = fast_try_fetching_and_executing(numPendingWorkRequests);\
			if (executedTaskCycles > 0) {\
				numLocallyRetiredTasks++;\
				totalLocalExecutedCycles += executedTaskCycles;\
			}\
		} \
		if (numLocallyRetiredTasks > 0) {\
			numRetiredTasks += numLocallyRetiredTasks;\
			totalExecutedTaskCycles += totalLocalExecutedCycles;\
		}\
	}

	#define submit_three_or_work(bigPacket, smallPacket, numPendingWorkRequests)  \
	{                                                               \
		int numLocallyRetiredTasks = 0;\
		unsigned long long totalLocalExecutedCycles = 0;\
		unsigned long long executedTaskCycles;\
		while (true) { \
			unsigned long long reply = submit_three_packets(bigPacket, smallPacket); \
			/*std::cerr << "[submit_three_or_work" << thread << "]: Just got signal" << reply << "." << std::endl;*/ \
			if (reply == 0) \
			break; \
			executedTaskCycles = try_fetching_and_executing(numPendingWorkRequests);\
			if (executedTaskCycles > 0) {\
				numLocallyRetiredTasks++;\
				totalLocalExecutedCycles += executedTaskCycles;\
			}\
		} \
		if (numLocallyRetiredTasks > 0) {\
			numRetiredTasks += numLocallyRetiredTasks;\
			totalExecutedTaskCycles += totalLocalExecutedCycles;\
		}\
	}
#else
	#define make_submission_request_or_work_fast(numPackets, swID, numPendingWorkRequests)  \
	{ \
		int numLocallyRetiredTasks = 0;\
		while (true) { \
			unsigned long long reply = make_submission_request(numPackets, swID); \
			/*std::cerr << "[make_submission_request_or_work, T" << thread << "]: Just got signal" << reply << "." << std::endl;*/ \
			if (reply == 0) \
			break; \
			if (fast_try_fetching_and_executing(numPendingWorkRequests)) {\
				numLocallyRetiredTasks++;\
			}\
		} \
		if (numLocallyRetiredTasks > 0) {\
			numRetiredTasks += numLocallyRetiredTasks;\
		}\
	}

	#define make_submission_request_or_work(numPackets, swID, numPendingWorkRequests)  \
	{ \
		int numLocallyRetiredTasks = 0;\
		while (true) { \
			unsigned long long reply = make_submission_request(numPackets, swID); \
			/*std::cerr << "[make_submission_request_or_work, T" << thread << "]: Just got signal" << reply << "." << std::endl;*/ \
			if (reply == 0) \
			break; \
			if (try_fetching_and_executing(numPendingWorkRequests)) {\
				numLocallyRetiredTasks++;\
			}\
		} \
		if (numLocallyRetiredTasks > 0) {\
			numRetiredTasks += numLocallyRetiredTasks;\
		}\
	}

	#define submit_or_work(packet, swID, numPendingWorkRequests) \
	{\
		int numLocallyRetiredTasks = 0;\
		while (true) {\
			unsigned long long reply = submit_packet(packet, swID);\
			/*std::cerr << "[submit_or_work]: Just got signal" << reply << "." << std::endl;*/\
			if (reply == 0)\
			break;\
			if (try_fetching_and_executing(numPendingWorkRequests)) {\
				numLocallyRetiredTasks++;\
			}\
		} \
		if (numLocallyRetiredTasks > 0) {\
			numRetiredTasks += numLocallyRetiredTasks;\
		}\
	}

	#define submit_three_or_work_fast(bigPacket, smallPacket, numPendingWorkRequests)  \
	{                                                               \
		int numLocallyRetiredTasks = 0;\
		while (true) { \
			unsigned long long reply = submit_three_packets(bigPacket, smallPacket); \
			/*std::cerr << "[submit_three_or_work" << thread << "]: Just got signal" << reply << "." << std::endl;*/ \
			if (reply == 0) \
			break; \
			if (fast_try_fetching_and_executing(numPendingWorkRequests)) {\
				numLocallyRetiredTasks++;\
			}\
		} \
		if (numLocallyRetiredTasks > 0) {\
			numRetiredTasks += numLocallyRetiredTasks;\
		}\
	}

	#define submit_three_or_work(bigPacket, smallPacket, numPendingWorkRequests)  \
	{                                                               \
		int numLocallyRetiredTasks = 0;\
		while (true) { \
			unsigned long long reply = submit_three_packets(bigPacket, smallPacket); \
			/*std::cerr << "[submit_three_or_work" << thread << "]: Just got signal" << reply << "." << std::endl;*/ \
			if (reply == 0) \
			break; \
			if (try_fetching_and_executing(numPendingWorkRequests)) {\
				numLocallyRetiredTasks++;\
			}\
		} \
		if (numLocallyRetiredTasks > 0) {\
			numRetiredTasks += numLocallyRetiredTasks;\
		}\
	}
#endif
